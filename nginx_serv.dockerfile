FROM nginx:alpine-slim as build

# VOLUME /html /nginx/html
# COPY nginx.conf /etc/nginx/nginx.conf
# EXPOSE 8080:80
# ENTRYPOINT [ "python3" ]
# CMD [ "somecode.py", "0.0.0.0:8000" ]

FROM scratch

WORKDIR /

COPY --from=build /etc/passwd /etc/group /etc/
COPY --from=build /etc/nginx /etc/nginx
# COPY --from=build /bin /bin
# COPY --from=build /usr /usr

# COPY --from=build /docker-entrypoint.sh /
# COPY --from=build /docker-entrypoint.d /docker-entrypoint.d
# COPY --from=build /usr/bin /usr/bin 
# COPY --from=build /usr/sbin /usr/sbin
# COPY --from=build /usr/lib /usr/lib
# COPY --from=build /usr/share/nginx /usr/share/nginx
#COPY index.html /usr/share/nginx/html/
#COPY ./config/nginx.conf /etc/nginx/nginx.conf

COPY --from=build /bin /bin                   
#COPY --from=build /docker-entrypoint.sh /docker-entrypoint.sh

# I try to don't copy all libs but image become larger
##COPY --from=build /lib/sysctl.d /lib/sysctl.d  
# COPY --from=build /lib/libz* /lib/   
##COPY --from=build /lib/libapk* /lib/ 
# COPY --from=build /lib/*musl* /lib/   
# COPY --from=build /lib/*ssl* /lib/  
# COPY --from=build /lib/libcrypto* /lib/

# COPY --from=build lib/libz* /lib/*musl* /lib/*ssl* /lib/libcrypto* /lib/

COPY --from=build /lib/ /lib/  

#COPY --from=build /opt /opt                   
COPY --from=build /run /run                   
#COPY --from=build /sys /sys              
COPY --from=build /var /var
#COPY --from=build /dev /dev
COPY --from=build /usr/lib /usr/lib                   
#COPY --from=build /etc /etc                   
#COPY --from=build /media /media                 
#COPY --from=build /proc /proc                  
#COPY --from=build /sbin /sbin                  
#COPY --from=build /tmp /tmp
#COPY --from=build /docker-entrypoint.d /docker-entrypoint.d   
#COPY --from=build /home /home                  
#COPY --from=build /mnt /mnt                   
#COPY --from=build /root /root                  
#COPY --from=build /srv /srv                   
COPY --from=build /usr/sbin/nginx /usr/sbin/nginx

# VOLUME ./html /usr/share/nginx/html/
# VOLUME ./config/nginx.conf /etc/nginx/conf.d/my_nginx.conf

#EXPOSE 8080:80

#ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;" ]

